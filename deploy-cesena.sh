#!/bin/sh

bundle exec jekyll build -d ../psicologo-cesena.gitlab.io/  
cd ../psicologo-cesena.gitlab.io/
git add .
CUR_DATE=`date`
cp sitemap.xml sitemap-cesena.xml
sed 's/psicologo-rimini/psicologo-cesena/g' -i sitemap-cesena.xml
git add .
git commit -m "Rilascio sito milena ${CUR_DATE}"
git push
cd ../milena
